import {mappings} from 'tramway-core-validation';
import joi from 'joi';

const {Mappings} = mappings;

const buildSchema = (schema, defaultValue, required = false) => {
    if (defaultValue) {
        schema.default(defaultValue);
    }
    
    if (required) {
        schema.required();
    }

    return schema;
};

export default {
    [Mappings.REGEXP]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.string().regex(value), defaultValue, required);
    },
    [Mappings.RANGE]: (value, options = {}, defaultValue, required = false) => {
        const [min, max] = value;
        return buildSchema(joi.number().min(min).max(max), defaultValue, required);
    },
    [Mappings.INT_RANGE]: (value, options = {}, defaultValue, required = false) => {
        const [min, max] = value;
        return buildSchema(joi.number().integer().min(min).max(max), defaultValue, required);
    },
    [Mappings.STRING]: (value, options = {}, defaultValue, required = false) => {
        const {minLength, maxLength, truncate, uppercase, lowercase} = options;

        let schema = joi.string();

        if (value) {
            schema.length(value);
        }

        if (minLength) {
            schema.min(minLength);
        }

        if (maxLength) {
            schema.max(maxLength);
        }

        if (truncate) {
            schema.truncate();
        }

        if (uppercase) {
            schema.uppercase();
        }

        if (lowercase) {
            schema.lowercase();
        }

        return buildSchema(schema, defaultValue, required);
    },
    [Mappings.NUMBER]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.number(), defaultValue, required)
    },
    [Mappings.EMAIL]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.string().email(options), defaultValue, required);
    },
    [Mappings.ENTITY]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.object().type(value), defaultValue, required);
    },
    [Mappings.CREDIT_CARD]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.string().creditCard(), defaultValue, required);
    },
    [Mappings.TOKEN]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.string().email(options), defaultValue, required);
    },
    [Mappings.IP]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.string().ip(options), defaultValue, required);
    },
    [Mappings.URI]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.string().uri(options), defaultValue, required);
    },
    [Mappings.GUID]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.string().guid(options), defaultValue, required);
    },
    [Mappings.ISO_DATE]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.string().isoDate(), defaultValue, required);
    },
    [Mappings.TIMESTAMP]: (value, options = {}, defaultValue, required = false) => {
        return buildSchema(joi.date().timestamp(value), defaultValue, required);
    },
    [Mappings.DATE]: (value, options = {}, defaultValue, required = false) => {
        const {min, max, greater, less} = options;

        let schema = joi.date();

        if (min) {
            schema.min(min);
        }

        if (max) {
            schema.max(max);
        }

        if (greater) {
            schema.greater(greater);
        }

        if (less) {
            schema.less(less);
        }

        return buildSchema(schema, defaultValue, required);
    },
    [Mappings.CUSTOM]: (value, options, defaultValue, required = false) => {
        return buildSchema(value, defaultValue, required);
    },
}