import joi from 'joi';
import {Schema, factories} from 'tramway-core-validation';
const {SchemaFactory} = factories;

export default class JoiSchemaFactory extends SchemaFactory {
    /**
     * 
     * @param {Schema} schema 
     */
    create(schema) {
        if (!(schema instanceof Schema)) {
            throw new Error('Expected valid Schema');
        }

        let joiSchema = joi.object();
        let objectKeys = {};

        for(let [key, schemaMeta] of Object.entries(schema)) {
            objectKeys[key] = this.mapper.getSchema(schemaMeta);
            schemaMeta.getAccompanies().forEach(accompanyingKey => joiSchema.with(key, accompanyingKey));
            schemaMeta.getExcludes().forEach(excludedKey => joiSchema.without(key, excludedKey));
        }

        return joiSchema.keys(objectKeys);
    }
}