import joi from 'joi';
import {providers, errors} from 'tramway-core-validation';

const {ValidationProvider} = providers;
const {InvalidSchemaError} = errors;

export default class JoiValidationProvider extends ValidationProvider {
    /**
     * 
     * @param {Entity} entity 
     * @param {Schema} schema 
     * @param {Object} options
     */
    validate(entity, schema, options = {}) {
        const {error, value} = joi.validate(entity, schema, options);

        if (error) {
            let invalidProperties = {};
            error.details && error.details.forEach((invalidProperty = {}) => {
                const {message, path} = invalidProperty;
                invalidProperties[path.join('.')] = message;
            })
            throw new InvalidSchemaError(error.message, invalidProperties);
        }

        return value;
    }
}